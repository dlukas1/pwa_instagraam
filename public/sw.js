self.addEventListener('install', function(event){
    console.log("[ServiceWorker]: installing...", event);
});

self.addEventListener('activate', function(event){
    console.log("[ServiceWorker]: activating...", event);
    return self.clients.claim();
});

self.addEventListener('fetch', function(event){
    console.log("[ServiceWorker]: fetching...", event);
    event.respondWith(fetch(event.request));
});