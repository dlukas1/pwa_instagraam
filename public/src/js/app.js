if(!window.Promise){
    window.Promise = Promise;
}

if('serviceWorker' in navigator){
    //register sw.js as a backgound process
    navigator.serviceWorker
    .register('/sw.js')
    .then(()=> console.log('ServiceWorker registered!'))
    .catch((err) => console.log(err));
}